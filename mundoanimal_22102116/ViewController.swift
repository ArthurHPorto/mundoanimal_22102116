//
//  ViewController.swift
//  mundoanimal_22102116
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Animal: Decodable {
    let name: String
    let latin_name: String
    let image_link: String
}

class ViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var latinName: UILabel!
    @IBOutlet var name: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAnimal()
    }
    
    func getAnimal() {
            AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
                response in
                if let animal = response.value {
                    self.imageView.kf.setImage(with: URL(string: animal.image_link))
                    self.latinName.text = animal.latin_name
                    self.name.title = animal.name
                }
            }
        }
}

